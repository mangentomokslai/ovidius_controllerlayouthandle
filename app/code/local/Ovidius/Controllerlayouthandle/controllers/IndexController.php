<?php
class Ovidius_Controllerlayouthandle_IndexController extends Mage_Core_Controller_Front_Action
{
    public function indexAction()
    {
        // load custom handle as decrared in layout xml and render layout
        $this->loadLayout(array('default', 'custom_layout_handle'))->renderLayout();
    }
}
